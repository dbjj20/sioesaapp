import React from 'react'
import Dexie from 'dexie'
// import bulma from 'bulma'
// import './style.css'
// export default function MyApp({ Component, pageProps }) {
//   return <Component {...pageProps} />
// }
export default class MyApp extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      dbs: undefined,
      db: undefined
    }
    this.createDb = this.createDb.bind(this)
    this.addtodbfriends = this.addtodbfriends.bind(this)
  }

  async createDb(name){
    if (name){
      console.log(name)
      let db = new Dexie(String(name));
      db.version(1).stores({
          friends: "++id, name, age, *tags"
      })
      await this.setState({db: db})

      console.log(this.state)
    } else {
      console.log('no Name in create db')
    }
  }

  async addtodbfriends(obj){
    // obj = name and age
    await this.state.db.friends.add(
      obj
    )
    console.log('friend added')
  }


  async componentDidMount(){
    let dbs = await Dexie.getDatabaseNames()

    if(dbs){
      let a = []
      dbs.forEach(async (item, i) => {
        a.push({[item]: new Dexie(item)})
      })
      await this.setState({dbs: a})
    }
    console.log(this.state)
  }

  render(){
    return(
      <this.props.Component
        {...this.props.pageProps}
        createDb={(name) => this.createDb(name)}
        addtodbfriends={(obj) => this.addtodbfriends(obj)}
        superState={this.state}
      />
    )
  }
}
