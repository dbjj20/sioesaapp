import React from 'react'

export default class Home extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      dbname: '',
      name: '',
      age: '',
      friends: []
    }
    this.handleInput = this.handleInput.bind(this)
    this.getfriends = this.getfriends.bind(this)
  }
  async getfriends(){
    let f = []
    await this.props.superState.db.friends.each(friend => f.push(friend))

    await this.setState({
      friends: f
    })
    console.log(this.state)
  }

  async handleInput(e){
    let {name, value} = e.target
    await this.setState({
      [name]: value
    })
  }

  componentDidMount(){

  }

  render(){
    return(
      <div>
      <h1>Friends CRUD</h1>
      <input name="dbname" onChange={this.handleInput} placeholder="db name"/>
      <button onClick={() => this.props.createDb(this.state.dbname)}>create db</button>
      <br/>
      {this.props.superState.db ? `Using data base named: ${this.props.superState.db.name}`:''}
      <br/>
      db list
      <hr/>
      {this.props.superState.dbs ? this.props.superState.dbs.map((db, i)=>{
        let d = Object.keys(db)[0]
        return (<button key={i} onClick={() => this.props.createDb(d)} >use {d}</button>)
      }) : ''}
      <br/>
      <hr/>
        friends goes here
        <li>{this.state.friends.map((f, i)=> {
          return (<div key={i}><br/><i key={i}>Name: {f.name}, Age: {f.age}</i> #id {f.id}<hr/></div>)
        })}</li>
        <button onClick={this.getfriends}>show friends</button>
      <br/>
      <hr/>
      <input name="name" onChange={this.handleInput} placeholder="friend name"/>
      <input name="age" onChange={this.handleInput} placeholder="friend age"/>
      <button onClick={() => this.props.addtodbfriends({name: this.state.name, age: this.state.age})}>add friend</button>
      </div>
    )
  }
}
